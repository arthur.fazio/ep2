#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "pilha.h"

/* O tipo posicao indica uma posicao de entrada numa matriz */
typedef struct {
  int linha;
  int coluna;
} posicao;

/* O tipo lacuna será utilizado para representar um conjunto de espaços contíguos (horizontal
ou verticalmente) disponíveis para o preenchimento de uma palavra */
typedef struct {
  posicao primeira_letra;
  char direcao;
  int comprimento;
} lacuna;

/* A função maximo recebe dois números inteiros a e b e devolve o máximo entre
esses dois inteiros*/
int maximo (int a, int b);

/* A função resolveCruzadas recebe uma cópia do tabuleiro de palavras cruzadas
em copia_cruzadas assim como suas respectivas dimensoes, a lista de palavras
a ser testada em lista_palavras, assim como seu tamanho e a lista com o tamanho
das palavras da lista e devolve 1, caso seja possível resolver as palavras
cruzadas e 0 caso contrário. No caso de conseguir resolver as palavras cruzadas,
a função modifica copia_cruzadas com a resolução.*/
int resolveCruzadas (char **copia_cruzadas, int m, int n, char **lista_palavras, int *tamanho_palavras, int l);

/* A função encontraLacuna recebe uma cópia do tabuleiro de palavras cruzadas
em copia_cruzadas, as sua dimensões, m e n, um vetor de lacunas espacos_disponiveis,
um ponteiro para a quantidade de lacunas já ocupadas no vetor espacos_disponiveis em
*tam, uma posicao do tabuleiro de palavras cruzadas em pos_inicial e um char orientacao.
Essa função procura o maior conjunto de espaços contíguos brancos horizontal ou verticalmente
(dependendo do valor parâmetro orientacao, que conterá 'h' ou 'v') com
a primeira posição livre igual a pos_inicial. Caso o encontre (se o comprimento
desse conjunto foir maior que 1), então a função acrescenta uma lacuna ao vetor,
espacos_disponiveis, aumenta seu tamanho, modificando o conteúdo da variável apontada por tam, e
devolve a primeira posição não branca depois da lacuna encontrada.*/
posicao encontraLacuna (char **copia_cruzadas, int m, int n, lacuna *espacos_disponiveis, int *tam, posicao pos_inicial, char orientacao);

/* A função encontraPosicaoValida procura, a partir da posicao_inicial o próximo espaço em branco disponível para o
preenchimento de algum caracter. A busca é feita de acordo com o parâmetro orientacao; se orientacao for 'h',
a busca é feita linha por linha, caso contrário, é feita coluna a coluna. */
posicao encontraPosicaoValida (char **copia_cruzadas, int m, int n, posicao posicao_inicial, char orientacao);

/* A função encontraTodasLacunas procura todas as lacunas formadas de mais de dois espaços
brancos contíguos no tabuleiro, sejam esses espaços contíguos na vertical, sejam esses espaços co
contíguos na horizontal. Essas lacunas não armazenadas no array espacos_disponiveis. */
void encontraTodasLacunas (char **copia_cruzadas, int m, int n, lacuna *espacos_disponiveis, int *tam);

/* A funçao podePreencherPalavra verifica se a entrada *palavra pode ser preenchida na lacuna l. Para isso,
 recebemos o tabuleiro copia_cruzadas, o tamanho dessa palavra em tamanho_palavra e um inteiro disponivel
 que informa se *palavra já foi colocada em alguma lacuna de copia_cruzadas. A função devolve
 1 caso a palavra possa ser colocada em lacuna e 0 caso contrário.*/
int podePreencherPalavra (char **copia_cruzadas, lacuna l, char *palavra, int tamanho_palavra, int disponivel);

/* A função preenchePalavra coloca os caracteres de *palavra nos campos correspondentes à lacuna l.*/
void preenchePalavra (char **copia_cruzadas, int **quantidade_letras, lacuna l, char *palavra, int tamanho_palavra);

/* A função tiraPalavra retira a palavra que está na lacuna l. Para isso, temos a matriz quantidade_letras, que
 armazena quantas vezes uma letra na entrada [i][j] da matriz copia_cruzadas foi utilizada. A matriz quantidade_letras
 impede que caracteres que são compartilhados com outras palavras sejam retirados da matriz copia_cruzadas.*/
void tiraPalavra (char **copia_cruzadas, int **quantidade_letras, lacuna l, int tamanho_palavra);
void liberaPonteiros (lacuna *espacos_disponiveis, pilha *decisoes, int **quantidade_letras, int m, int *palavra_disponivel);

/* A função temPosicaoImpossivel procura por posições em branco no tabuleiro que
estejam ceracadas em cima, à direita, em baixo, e à esquerda por posições que
não podem ser preenchidas por caracteres, seja porque é uma posição
fora do tabuleiro, seja porque a posição está preenchida com *.*/
int temPosicaoImpossivel (char **copia_cruzadas, int m, int n);

/* A função posicaoImpossivel determina se a entrada i j do tabuleiro é
uma posição cercada de posições que não podem ser preenchidas por caracteres.*/
int posicaoImpossivel (char **copia_cruzadas, int m, int n, int i, int j);

/* A função lacunasSeCruzam recebe duas lacunas com direcoes diferentes do tabuleiro copia_cruzadas, lacuna1 e lacuna2.
 A função devolve 1 caso as palavras compartilhem alguma entrada (se cruzam) e 0 caso contrário.
 É importante ressaltar que uma das lacunas recebidas deve estar em direção horizontal e a outra, vertical,
 caso contrário, a função pode devolver valores errados. No programa ela só é utilizada caso essa condição esteja
 satisfeita.*/
int lacunasSeCruzam (lacuna lacuna1, lacuna lacuna2);

/* A função reorganizaArrayEspacos reorganiza o vetor espacos_disponiveis com o objetivo de melhorar a ordem de preenchimento
 das lacunas no backtracking. Isso porque no array espacos_disponiveis, momento do recebimento, existe algum índice i
 para o qual todas as entradas com índice menor que i são lacunas horizontais e todas as entradas com índice maior ou
 igual a i são lacunas verticais. A função reorganizaArrayEspacos então organiza as lacunas numa estrutura anáoga a um heap:
 primeiro escolhemos uma certa lacuna; seguida dela, estarão todas as lacunas que cruzam com ela; depois dessas, estarão
 as lacunas que cruzam com essas últimas e assim por diante. A função reorganizaArrayEspacos foi também escrita para que
 não haja repetiçoes de lacunas no vetor reorganizado.*/
void reorganizaArrayEspacos (lacuna *espacos_disponiveis, int tam);

int main () {
  int m, n, instancia;
  clock_t t; /*TIRA */
  scanf ("%d %d", &m, &n);
  instancia = 0;
  t = clock (); /*TIRA */

  while (m != 0 && n != 0) {
    int **cruzadas;
    char **copia_cruzadas;
    int *tamanho_palavras;
    int i, j, l, maior_dimensao, eh_possivel;
    char **lista_palavras;
    instancia += 1;
    cruzadas = malloc (m * sizeof (int *));
    copia_cruzadas = malloc (m * sizeof (char *));
    for (i = 0; i < m; i++) {
      cruzadas[i] = malloc (n * sizeof (int));
      copia_cruzadas[i] = malloc (n * sizeof (char));
      for (j = 0; j < n; j++) {
        scanf ("%d", &cruzadas[i][j]);
        if (cruzadas[i][j] == 0)
          copia_cruzadas[i][j] = '0';
        else
          copia_cruzadas[i][j] = '*';
      }
    }

    scanf ("%d", &l);
    lista_palavras = malloc (l * sizeof (char *));
    tamanho_palavras = malloc (l * sizeof (int));
    maior_dimensao = maximo (m, n);
    for (i = 0; i < l; i++) {
      lista_palavras[i] = malloc (maior_dimensao * sizeof (char) + 1);
      scanf ("%s", lista_palavras[i]);
      tamanho_palavras[i] = strlen (lista_palavras[i]);
    }

    eh_possivel = resolveCruzadas (copia_cruzadas, m, n, lista_palavras, tamanho_palavras, l);

    printf ("Instancia %d\n", instancia);
    if (eh_possivel) {
      for (i = 0; i < m; i++) {
        for (j = 0; j < n; j++) {
          printf ("%c ", copia_cruzadas[i][j]);
        }
        printf ("\n");
      }
    }
    else {
      printf ("nao ha solucao\n");
    }

    for (i = 0; i < m; i++) {
      free (cruzadas[i]);
      cruzadas[i] = NULL;
      free (copia_cruzadas[i]);
      copia_cruzadas[i] = NULL;
    }
    free (cruzadas);
    cruzadas = NULL;
    free (copia_cruzadas);
    copia_cruzadas = NULL;
    for (i = 0; i < l; i++) {
      free (lista_palavras[i]);
      lista_palavras[i] = NULL;
    }
    free (lista_palavras);
    lista_palavras = NULL;
    free (tamanho_palavras);
    tamanho_palavras = NULL;

    scanf ("%d %d", &m, &n);
  }
  return 0;
}

int maximo (int a, int b) {
  if (a >= b)
    return a;
  else
    return b;
}

int resolveCruzadas (char **copia_cruzadas, int m, int n, char **lista_palavras, int *tamanho_palavras, int l) {
  lacuna *espacos_disponiveis;
  pilha *decisoes;
  int **quantidade_letras, *palavra_disponivel;
  int tam, i ,j, achei, palavra_a_testar, lacuna_a_preencher;

  if (temPosicaoImpossivel (copia_cruzadas, m, n)) {
    return 0;
  }

  espacos_disponiveis = malloc (2 * m * n * sizeof (lacuna));
  tam = 0;
  encontraTodasLacunas (copia_cruzadas, m, n, espacos_disponiveis, &tam);
  reorganizaArrayEspacos (espacos_disponiveis, tam);

  quantidade_letras = malloc (m * sizeof (int *));
  for (i = 0; i < m; i++) {
    quantidade_letras[i] = malloc (n * sizeof (int));
    for (j = 0; j < n; j++) {
      if (copia_cruzadas[i][j] == '0') {
        quantidade_letras[i][j] = 0;
      }
      else {
        quantidade_letras[i][j] = -1;
      }
    }
  }
  palavra_disponivel = malloc (l * sizeof (int));
  for (i = 0; i < l; i++) {
    palavra_disponivel[i] = 1;
  }

  decisoes = criaPilha (tam);
  lacuna_a_preencher = 0;
  palavra_a_testar = 0;
  while (lacuna_a_preencher < tam) {
    achei = 0;
    while (!achei && palavra_a_testar < l) {
      if (podePreencherPalavra (copia_cruzadas, espacos_disponiveis[lacuna_a_preencher], lista_palavras[palavra_a_testar], tamanho_palavras[palavra_a_testar], palavra_disponivel[palavra_a_testar])){
        achei = 1;
      }
      else {
        palavra_a_testar += 1;
      }
    }
    if (achei) {
      empilha (decisoes, palavra_a_testar);
      palavra_disponivel[palavra_a_testar] = 0;
      preenchePalavra (copia_cruzadas, quantidade_letras, espacos_disponiveis[lacuna_a_preencher], lista_palavras[palavra_a_testar], tamanho_palavras[palavra_a_testar]);
      lacuna_a_preencher += 1;
      palavra_a_testar = 0;
    }
    else {
      if (pilhaVazia (decisoes)) {
        liberaPonteiros (espacos_disponiveis, decisoes, quantidade_letras, m, palavra_disponivel);
        return 0;
      }
      palavra_a_testar = desempilha (decisoes);
      lacuna_a_preencher -= 1;
      tiraPalavra (copia_cruzadas, quantidade_letras, espacos_disponiveis[lacuna_a_preencher], tamanho_palavras[palavra_a_testar]);
      palavra_disponivel[palavra_a_testar] = 1;
      palavra_a_testar += 1;
    }
  }
  liberaPonteiros (espacos_disponiveis, decisoes, quantidade_letras, m, palavra_disponivel);
  return 1;
}

posicao encontraPosicaoValida (char **copia_cruzadas, int m, int n, posicao posicao_inicial, char orientacao) {
  posicao posicao_valida;
  int linha_inicial, coluna_inicial, i, j;
  linha_inicial = posicao_inicial.linha;
  coluna_inicial = posicao_inicial.coluna;
  if (linha_inicial >= m) {
    if (orientacao == 'h') {
      posicao_valida.linha = -1;
      posicao_valida.coluna = -1;
      return posicao_valida;
    }
    else {
      linha_inicial = 0;
      coluna_inicial += 1;
    }
  }
  if (coluna_inicial >= n) {
    if (orientacao == 'v') {
      posicao_valida.linha = -1;
      posicao_valida.coluna = -1;
      return posicao_valida;
    }
    else {
      linha_inicial += 1;
      coluna_inicial = 0;
    }
  }

  i = linha_inicial;
  j = coluna_inicial;
  if (orientacao == 'h') {
    while (i < m) {
      while (j < n) {
        if (copia_cruzadas[i][j] == '0') {
          posicao_valida.linha = i;
          posicao_valida.coluna = j;
          return posicao_valida;
        }
        j += 1;
      }
      j = 0;
      i += 1;
    }
  }
  else {
    while (j < n) {
      while (i < m) {
        if (copia_cruzadas[i][j] == '0') {
          posicao_valida.linha = i;
          posicao_valida.coluna = j;
          return posicao_valida;
        }
        i += 1;
      }
      i = 0;
      j += 1;
    }
  }
  posicao_valida.linha = -1;
  posicao_valida.coluna = -1;
  return posicao_valida;
}

posicao encontraLacuna (char **copia_cruzadas, int m, int n, lacuna *espacos_disponiveis, int *tam, posicao pos_inicial, char orientacao) {
  int i, j, tamanho;
  posicao posicao_invalida;
  i = pos_inicial.linha;
  j = pos_inicial.coluna;
  tamanho = 0;

  while (i >= 0 && i < m && j >= 0 && j < n && copia_cruzadas[i][j] == '0') {
    if (orientacao == 'h')
      j += 1;
    else
      i += 1;
    tamanho += 1;
  }
  posicao_invalida.linha = i;
  posicao_invalida.coluna = j;

  if (tamanho > 1) {
    lacuna novo_espaco;
    novo_espaco.primeira_letra = pos_inicial;
    novo_espaco.direcao = orientacao;
    novo_espaco.comprimento = tamanho;
    espacos_disponiveis[*tam] = novo_espaco;
    *tam += 1;
  }

  return posicao_invalida;
}

void encontraTodasLacunas (char **copia_cruzadas, int m, int n, lacuna *espacos_disponiveis, int *tam) {
  posicao p;
  p.linha = 0;
  p.coluna = 0;
  while (p.linha != -1 && p.coluna != -1) {
    p = encontraPosicaoValida (copia_cruzadas, m, n, p, 'h');
    p = encontraLacuna (copia_cruzadas, m, n, espacos_disponiveis, tam, p, 'h');
  }
  p.linha = 0;
  p.coluna = 0;
  while (p.linha != -1 && p.coluna != -1) {
    p  = encontraPosicaoValida (copia_cruzadas, m, n, p, 'v');
    p = encontraLacuna (copia_cruzadas, m, n, espacos_disponiveis, tam, p, 'v');
  }
  return;
}

int podePreencherPalavra (char **copia_cruzadas, lacuna l, char *palavra, int tamanho_palavra, int disponivel) {
  int i;
  posicao letra;
  if (l.comprimento != tamanho_palavra || !disponivel) {
    return 0;
  }
  letra.linha = l.primeira_letra.linha;
  letra.coluna = l.primeira_letra.coluna;
  for (i = 0; i < tamanho_palavra; i++) {
    if (copia_cruzadas[letra.linha][letra.coluna] != '0' && copia_cruzadas[letra.linha][letra.coluna] != palavra[i]) {
      return 0;
    }
    if (l.direcao == 'h') {
      letra.coluna += 1;
    }
    else {
      letra.linha += 1;
    }
  }
  return 1;
}

void preenchePalavra (char **copia_cruzadas, int **quantidade_letras, lacuna l, char *palavra, int tamanho_palavra) {
  int i;
  posicao letra;
  letra.linha = l.primeira_letra.linha;
  letra.coluna = l.primeira_letra.coluna;
  for (i = 0; i < tamanho_palavra; i++) {
    copia_cruzadas[letra.linha][letra.coluna] = palavra[i];
    quantidade_letras[letra.linha][letra.coluna] += 1;
    if (l.direcao == 'h') {
      letra.coluna += 1;
    }
    else {
      letra.linha += 1;
    }
  }
  return;
}

void tiraPalavra (char **copia_cruzadas, int **quantidade_letras, lacuna l, int tamanho_palavra) {
  int i;
  posicao letra;
  letra.linha = l.primeira_letra.linha;
  letra.coluna = l.primeira_letra.coluna;
  for (i = 0; i < tamanho_palavra; i++) {
    quantidade_letras[letra.linha][letra.coluna] -= 1;
    if (quantidade_letras[letra.linha][letra.coluna] == 0) {
      copia_cruzadas[letra.linha][letra.coluna] = '0';
    }
    if (l.direcao == 'h') {
      letra.coluna += 1;
    }
    else {
      letra.linha += 1;
    }
  }
  return;
}

void liberaPonteiros (lacuna *espacos_disponiveis, pilha *decisoes, int **quantidade_letras, int m, int *palavra_disponivel) {
  int i;
  free (espacos_disponiveis);
  espacos_disponiveis = NULL;
  destroiPilha (decisoes);
  decisoes = NULL;
  for (i = 0; i < m; i++) {
    free (quantidade_letras[i]);
    quantidade_letras[i] = NULL;
  }
  free (quantidade_letras);
  quantidade_letras = NULL;
  free (palavra_disponivel);
  palavra_disponivel = NULL;
  return;
}

int temPosicaoImpossivel (char **copia_cruzadas, int m, int n) {
  int i, j;
  for (i = 0; i < m; i++) {
    for (j = 0; j < n; j++) {
      if (copia_cruzadas[i][j] == '0' && posicaoImpossivel (copia_cruzadas, m, n, i, j)) {
        return 1;
      }
    }
  }
  return 0;
}

int posicaoImpossivel (char **copia_cruzadas, int m, int n, int i, int j) {
  if (i - 1 >= 0 && copia_cruzadas[i - 1][j] != '*') {
    return 0;
  }
  if (j + 1 < n && copia_cruzadas[i][j + 1] != '*') {
    return 0;
  }
  if (i + 1 < m && copia_cruzadas[i + 1][j] != '*') {
    return 0;
  }
  if (j - 1 >= 0 && copia_cruzadas[i][j - 1] != '*') {
    return 0;
  }
  return 1;
}

int lacunasSeCruzam (lacuna lacuna1, lacuna lacuna2) {
  lacuna lacunavertical, lacunahorizontal;
  if (lacuna1.direcao == 'v') {
    lacunavertical = lacuna1;
    lacunahorizontal = lacuna2;
  }
  else {
    lacunavertical = lacuna2;
    lacunahorizontal = lacuna1;
  }
  if (lacunavertical.primeira_letra.linha <= lacunahorizontal.primeira_letra.linha &&
      lacunahorizontal.primeira_letra.linha < lacunavertical.primeira_letra.linha + lacunavertical.comprimento &&
      lacunahorizontal.primeira_letra.coluna <= lacunavertical.primeira_letra.coluna &&
      lacunavertical.primeira_letra.coluna < lacunahorizontal.primeira_letra.coluna + lacunahorizontal.comprimento) {
     return 1;
  }
  return 0;
}

void reorganizaArrayEspacos (lacuna *espacos_disponiveis, int tam) {
  lacuna *espacos_disponiveis_melhorado;
  int indice_primeira_lacuna_vertical, quantos_ja_reorganizados, *lacuna_ja_reorganizada;
  int i, j;
  espacos_disponiveis_melhorado = malloc (tam * sizeof (lacuna));
  indice_primeira_lacuna_vertical = 0;
  while (indice_primeira_lacuna_vertical < tam &&
         espacos_disponiveis[indice_primeira_lacuna_vertical].direcao != 'v') {
        indice_primeira_lacuna_vertical += 1;
  }
  lacuna_ja_reorganizada = malloc (tam * sizeof (int));
  for (i = 0; i < tam; i++) {
    lacuna_ja_reorganizada[i] = 0;
  }
  i = 0;
  espacos_disponiveis_melhorado[0] = espacos_disponiveis[0];
  lacuna_ja_reorganizada[0] = 1;
  quantos_ja_reorganizados = 1;
  while (quantos_ja_reorganizados < tam) {
    if (espacos_disponiveis_melhorado[i].direcao == 'h') {
      for (j = indice_primeira_lacuna_vertical; j < tam; j++) {
        if (!lacuna_ja_reorganizada[j] && lacunasSeCruzam (espacos_disponiveis_melhorado[i], espacos_disponiveis[j])) {
          espacos_disponiveis_melhorado[quantos_ja_reorganizados] = espacos_disponiveis[j];
          lacuna_ja_reorganizada[j] = 1;
          quantos_ja_reorganizados += 1;
        }
      }
    }
    else {
      for (j = 0; j < indice_primeira_lacuna_vertical; j++) {
        if (!lacuna_ja_reorganizada[j] && lacunasSeCruzam (espacos_disponiveis_melhorado[i], espacos_disponiveis[j])) {
          espacos_disponiveis_melhorado[quantos_ja_reorganizados] = espacos_disponiveis[j];
          lacuna_ja_reorganizada[j] = 1;
          quantos_ja_reorganizados += 1;
        }
      }
    }
    if (i < quantos_ja_reorganizados) {
      i += 1;
    }
    else if (quantos_ja_reorganizados < tam) {
      i = 0;
      while (i < tam && lacuna_ja_reorganizada[i]) {
        i += 1;
      }
      espacos_disponiveis_melhorado[quantos_ja_reorganizados] = espacos_disponiveis[i];
      lacuna_ja_reorganizada[i] = 1;
      quantos_ja_reorganizados += 1;
    }
  }
  for (i = 0; i < tam; i++) {
    espacos_disponiveis[i] = espacos_disponiveis_melhorado[i];
  }
  free (lacuna_ja_reorganizada);
  lacuna_ja_reorganizada = NULL;
  free (espacos_disponiveis_melhorado);
  espacos_disponiveis_melhorado = NULL;
  return;
}
