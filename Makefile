all: ep2

ep2: ep2.c pilha.c
	gcc -Wall -ansi -pedantic -O2 ep2 *.c

clean: ep2
	rm ep2
